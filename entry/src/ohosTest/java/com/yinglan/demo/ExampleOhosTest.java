package com.yinglan.demo;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.yinglan.demo", actualBundleName);
    }
    @Test
    public void testBundleName2() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.yinglan.demo", actualBundleName);
    }

    @Test
    public void testBundleName3() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.yinglan.demo", actualBundleName);
    }
    @Test
    public void testBundleName4() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.yinglan.demo", actualBundleName);
    }
    @Test
    public void testBundleName5() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.yinglan.demo", actualBundleName);
    }



}