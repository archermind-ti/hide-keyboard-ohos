# HideKeyboard

#### 简介
仿iOS输入法点击输入框以外区域 自动隐藏软键盘轻量级库 ， Imitation iOS automatic hidden soft keyboard

#### 功能
- 仿iOS输入法点击输入框以外区域 自动隐藏软键盘轻量级库

#### 演示
<img src="./demo/demo.gif" width="40%" height="40%">

#### 集成

方式一

1. 下载har包[HideKeyboard_ohos.har](https://gitee.com/archermind-ti/hide-keyboard-ohos/releases/1.0.0)
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。


```
 dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
 }
```


方式二

在project的build.gradle中添加mavenCentral()的引用


```
 repositories {   
 	...   
 	mavenCentral()   
	...           
 }
```

在entry的build.gradle中添加依赖


```
dependencies { 
    ... 
    implementation 'com.gitee.archermind-ti:hidekeyboard_ohos:1.0.2' 
    ... 
}
```



#### 使用说明

收起输入法软件盘用法，参数分别为
1. 当前的AbilitySlice
2. 根布局的id

```
    KeyboardUtils.HideUtil(this,ResourceTable.Id_root_layout);
```

#### 编译说明
- 1.将项目通过git clone 至本地
- 2.使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
- 3.点击Run运行即可（真机运行可能需要配置签名）

#### 版本迭代
- 1.0.0
- 1.0.2 修改了包名

#### 版权和许可信息

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
