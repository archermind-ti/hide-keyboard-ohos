/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yinglan.keyboard;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

public class KeyboardUtils {

    public static void HideUtil(final AbilitySlice abilitySlice, int rootId) {
        Component component = abilitySlice.findComponentById(rootId);
        component.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                dispatchTouchEvent(component, touchEvent);
                return false;
            }
        });
    }

    private static boolean dispatchTouchEvent(Component component, TouchEvent ev) {
        if (ev.getAction() == TouchEvent.PRIMARY_POINT_DOWN && isShouldHideInput(component, ev)) {
            clearFocus(component);
        }
        return false;
    }

    /**
     * @param component
     * @param event
     * @return
     */
    private static boolean isShouldHideInput(Component component, TouchEvent event) {
        MmiPoint point = event.getPointerPosition(event.getIndex());
        float x = point.getX();
        float y = point.getY();
        if (component instanceof TextField) {
            float contentPositionX = component.getContentPositionX();
            float contentPositionY = component.getContentPositionY();
            float cX = contentPositionX + component.getWidth();
            float cY = contentPositionY + component.getHeight();
            if ((x > contentPositionX && x < cX) && (y > contentPositionY && y < cY)) {
                return false;
            }
        }
        return true;
    }

    private static void clearFocus(Component component) {
        Component focusedComponent = findFocus(component, Component.FOCUS_NEXT);//尝试向后找焦点控件
        if (focusedComponent != null) {
            focusedComponent.clearFocus();
            return;
        }

        focusedComponent = findFocus(component, Component.FOCUS_PREVIOUS);//尝试向前找焦点控件
        if (focusedComponent != null) {
            focusedComponent.clearFocus();
        }
    }

    //找焦点控件
    private static Component findFocus(Component component, int direction) {
        if (component.hasFocus()) {
            return component;
        }
        Component focusableComponent = component;
        int i = 99;
        while (i-- > 0) {
            focusableComponent = focusableComponent.findNextFocusableComponent(direction);
            if (focusableComponent != null) {
                if (focusableComponent.hasFocus()) {
                    return focusableComponent;
                }
            } else {
                break;
            }
        }
        return null;
    }
}
